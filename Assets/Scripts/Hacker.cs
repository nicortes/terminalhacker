﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hacker : MonoBehaviour
{
    //Variables
    string playerName = "";
    int level = 0;

    string menuHint = "Puedes volver ingresando: ";
    string goToMenuWord = "volver";

    string asteriskLine = "**************************************";
    string dashLine = "--------------------------------------";

    enum Screen {MainMenu, PlayerName, Password, Win, Loose};
    Screen currentScreen;

    string password;
    string[] level1Passwords = {"wifi","vecino","módem","contraseña","router"};
    string[] level2Passwords = {"libro", "profesor", "notas", "aprobado", "reprobado" };
    string[] level3Passwords = {"Luksic", "tarjeta de crédito", "cuenta corriente", "cuenta de ahorro", "préstamo"};

    // Las variables constantes no pueden ser modificadas en tiempo de ejecución
    // También hacen más rápida la ejecución del código
    //const string menuHint = "Puedes volver ingresando: volver";
    
    // Start is called before the first frame update
    void Start() {
        /*
        // Estas dos son lo mismo, pero print() hereda de MonoBehaviour
        // Por eso se usa sin el nombre de la clase
        // Si la clase no hereda de MonoBehaviour, la única opción es Debug.Log()
        print("Esto lo hice con print");
        Debug.Log("Esto lo hice con Debug.Log");

        // Sólo escribe en la Editor-Logfile y no en la consola
        System.Console.WriteLine("Esto lo hice con System.Console.Writeline");

        // Exclusivo del package descargado del curso
        Terminal.WriteLine("Esto lo hice con Terminal.WriteLine");
        */
        AskName();
    }

    void AskName() {
        currentScreen = Screen.PlayerName;
        Terminal.WriteLine("Ingresa tu nombre: ");
    }

    void OnUserInput(string input) {
        if (currentScreen == Screen.PlayerName) {
            playerName = input;
            ShowMainMenu();
        } 
        else if (input == goToMenuWord) {
            ShowMainMenu();
        } 
        else if (currentScreen == Screen.MainMenu) {
            RunMainMenu(input);
        } 
        else if (currentScreen == Screen.Password) {
            CheckPassword(input);
        } 
        else if(currentScreen == Screen.Win) {
            DisplayWinScreen();
        } 
        else if(currentScreen == Screen.Loose) {
            DisplayLooseScreen();
        }
        else {
            Terminal.WriteLine("Error. Ingresa: volver");
        }
    }

    void ShowMainMenu() {
        currentScreen = Screen.MainMenu;
        Terminal.ClearScreen();
        Terminal.WriteLine("Bienvenido/a " + playerName);
        Terminal.WriteLine(asteriskLine);
        Terminal.WriteLine("¿Qué deseas hackear hoy?");
        Terminal.WriteLine(asteriskLine);
        Terminal.WriteLine("1) El wifi del vecino.");
        Terminal.WriteLine("2) Las notas del curso.");
        Terminal.WriteLine("3) El Banco de Chile.");
        Terminal.WriteLine(dashLine);
        Terminal.WriteLine("Ingresa su número: ");
    }

    private void RunMainMenu(string input) {
        bool isValidLevelNumber;
        isValidLevelNumber = (input == "1" || input == "2" || input == "3");

        if (isValidLevelNumber) {

            level = int.Parse(input);

            CheckLevel();

        } else {
            ShowMainMenu();
            //DisplayWrongInputMessage();
        }
    }

    private void CheckPassword(string input) {
        if (input == password) {
            DisplayWinScreen();
        } else {
            DisplayLooseScreen();
        }
    }
    
    void DisplayWinScreen() {
        currentScreen = Screen.Win;
        ShowLevelReward();
    }

    void DisplayLooseScreen() {
        currentScreen = Screen.Loose;

        StartGame("Hackeo fallido...");
    }
    
    void StartGame(string header) {
        currentScreen = Screen.Password;

        SetRandomPassword();

        Terminal.ClearScreen();
        Terminal.WriteLine(header);
        Terminal.WriteLine(asteriskLine);
        Terminal.WriteLine("Reordena las letras para obtener");
        Terminal.WriteLine("la clave: " + password.Anagram());
        Terminal.WriteLine(asteriskLine);
        Terminal.WriteLine(menuHint + goToMenuWord);
        Terminal.WriteLine(dashLine);
        Terminal.WriteLine("Ingresa la clave: ");
    }

    void CheckLevel() {
        if (level == 1) {
            StartGame("Hackeando el wifi del vecino...");
        } else if (level == 2) {
            StartGame("Hackeando las notas del curso...");
        } else if (level == 3) {
            StartGame("Hackeando el Banco de Chile...");
        }
        ;
    }

    void ShowLevelReward() {

        Terminal.ClearScreen();
        Terminal.WriteLine("¡Hackeo exitoso!");

        if (level == 1) {
            Terminal.WriteLine("Disfruta de wifi para tu casa.");
            Terminal.WriteLine(@"
  ____||____
 ///////////\
///////////  \
|    _    |  |
|   | |   |  |
                    ");
        }
        if (level == 2) {
            Terminal.WriteLine("Aprobaste todo sin esfuerzo.");
            Terminal.WriteLine(@"
  ,-' -.
, '  .----.   _________
`.     , ' ) (@)__))___)
 |`-.- '| #       \\
  `---'           ^     hjw
                    ");
        }
        if (level == 3) {
            Terminal.WriteLine("Pero al banco no le gustó.");
            Terminal.WriteLine(playerName + "... ¡Corre!");
            Terminal.WriteLine(@"
 .d888b.
 8888888                ____
 `Y888P' `=-=-=-=-=-=-<|____|                
                    ");
        }
        //Terminal.WriteLine(asteriskLine);
        Terminal.WriteLine(menuHint + goToMenuWord);
        Terminal.WriteLine(dashLine);
    }
    
    void SetRandomPassword() {

        int amountOfPasswords;
        int randomNumber;

        switch (level) {
            case 1:
                amountOfPasswords = level1Passwords.Length;
                randomNumber = Random.Range(0, amountOfPasswords);
                password = level1Passwords[randomNumber];
                break;
            case 2:
                amountOfPasswords = level2Passwords.Length;
                randomNumber = Random.Range(0, amountOfPasswords);
                password = level2Passwords[randomNumber];
                break;
            case 3:
                amountOfPasswords = level3Passwords.Length;
                randomNumber = Random.Range(0, amountOfPasswords);
                password = level3Passwords[randomNumber];
                break;
            default:
                Debug.LogError("Número de nivel incorrecto.");
                break;
        }
    }

// Update is called once per frame
void Update()
    {
        
    }

    
}
